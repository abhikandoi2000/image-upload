var Jimp = require("jimp");

var ImageCategories = {
    HORIZONTAL: 'HORIZONTAL',
    VERTICAL: 'VERTICAL',
    HORIZONTAL_SMALL: 'HORIZONTAL_SMALL',
    GALLERY: 'GALLERY'
};

var ImageSizes = {
    'HORIZONTAL': {w: 755, h: 450},
    'VERTICAL': {w: 365, h: 450},
    'HORIZONTAL_SMALL': {w: 365, h: 212},
    'GALLERY': {w: 380, h: 380}
};

var ImageStore = {
    put: function(userid, category, image) {
        return new Promise((resolve, reject) => {
            let extension = 'jpeg';
            let filename = [category.toLowerCase(), extension].join('.');
            let filepath = './uploads/' + userid + '/' + filename;

            FileSystemStore.save(filepath, image).then(image => {
                resolve();
            }).catch(error => {
                reject(error);
            });
        });
    }
};

var FileSystemStore = {
    save: function(filepath, image) {
        return new Promise((resolve, reject) => {
            image.write(filepath, function(err, image) {
                if(err) {
                    reject(err);
                }

                resolve(image);
            });
        });
    }
};

var ImageUploadHandler = {
    upload: function(userid, image) {
        return new Promise((resolve, reject) => {
            // 1. store original
            let p1 = ImageUploadHandler.storeOriginal(userid, image);
            // 2. convert and store for each category
            let p2 = ImageUploadHandler.storeResizedCopies(userid, image);

            Promise.all([p1, p2]).then(results => {
                resolve();
            }).catch((err) => {
                reject(err);
            });
        });
    },

    storeResizedCopies: async function(userid, image) {
        let categories = [
            ImageCategories.HORIZONTAL,
            ImageCategories.VERTICAL,
            ImageCategories.HORIZONTAL_SMALL,
            ImageCategories.GALLERY
        ];

        for (category of categories) {
            let size = ImageSizes[category];

            let resized = await ImageUtils.cover(image, size);

            await ImageStore.put(userid, category, resized);
        }
    },

    storeOriginal: function(userid, image) {
        return new Promise(function(resolve, reject) {
            Jimp.read(image.buffer).then(image => {
                ImageStore.put(userid, 'ORIGINAL', image);
                resolve();
            }).catch(err => {
                reject(err);
            });
        });
    }
}

var ImageUtils = {
    cover: function(image, newsize) {
        return new Promise((resolve, reject) => {
            Jimp.read(image.buffer).then(image => {
                let {w, h} = newsize;
                image.cover(w, h);
                resolve(image);
            }).catch(err => {
                reject(err);
            });
        });
    }
}

module.exports = {
    ImageStore: ImageStore,
    ImageUploadHandler: ImageUploadHandler,
    ImageUtils: ImageUtils
}
