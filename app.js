var express = require('express');
var multer  = require('multer');
const {ImageUploadHandler} = require('./imagestore');

var app = express();

app.set('views', './views');
app.set('view engine', 'jade');
app.set('port', (process.env.PORT || 8080));

app.use(express.static('uploads'));
app.use(express.static('public'));

var storage = multer.memoryStorage();
var fileFilter = function(req, file, cb) {
    let mimetypes = ["image/png", "image/jpg", "image/jpeg"];
    if (mimetypes.indexOf(file.mimetype) < 0) {
        cb(null, false);
    }

    cb(null, true);
}
var options = {
    storage: storage,
    limits: {
        fileSize: 5 * 1024 * 1024
    },
    fileFilter: fileFilter
};
var upload = multer(options).single('img');

app.get('/', function(req, res) {
    res.render('home');
});

app.get('/uploads', function(req, res) {
    res.render('uploads');
});

app.post('/upload', function(req, res) {
    upload(req, res, function (err) {
        if (err) {
            if (err.message === 'File too large') {
                res.send('File too large. Max file size is 5MB.');
            } else {
                console.error(err);
                res.send('Error occurred while uploading');
            }
            return;
        }

        let image = req.file;
        let userid = req.body['userid'];

        ImageUploadHandler.upload(userid, image).then(() => {
            res.render('upload');
        }).catch(err => {
            console.error(err);
            res.send('Error occurred while uploading');
        });
      })
});

app.listen(app.get('port'), "0.0.0.0", function() {
    console.log('listening on port', app.get('port'));
});
