var Utils = {
    validateSize: function(file) {
        return new Promise(function(resolve, reject) {
            img = new Image();
            img.onload = function () {
                let {width, height} = this;
                if (width !== 1024 || height !== 1024) {
                    resolve(false);
                }
                resolve(true);
            };
            img.onerror = function(error) {
                reject(error);
            };
            img.src = window.URL.createObjectURL(file);
        });
    }
};

var App = {
    init: function() {
        let $form = document.getElementById('imgform');
        $form.addEventListener('submit', App.handleUpload);

        let $img = document.getElementById('img');
        $img.addEventListener('change', App.validateFile);
    },

    handleUpload: function(evt) {
        evt.preventDefault();

        let $img = document.getElementById('img');
        let file = $img.files[0];

        if (file === undefined) {
            alert('Select an image to upload.');
        }

        Utils.validateSize(file).then((valid) => {
            if (!valid) {
                alert('Image should be 1024x1024 in size.');
            } else {
                // submit form
                this.submit();
            }
        });
    },

    validateFile: function(evt) {
        let file, img;
        if ((file = this.files[0])) {
            Utils.validateSize(file).then((valid) => {
                if (!valid) {
                    let $error = document.querySelector('.error-msg');
                    $error.textContent = 'Image should be 1024x1024 in size.';
                }
            }).catch(err => {
                this.value = '';
                alert('Non image files are not supported');
            });
        }
    }
};

var initialize =  function() {
    App.init();
};

document.addEventListener('DOMContentLoaded', initialize, false);
